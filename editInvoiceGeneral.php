<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$projectList = getProject($conn);
$purchaserDetails = getLoanStatus($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Invoice | GIC" />
    <title>Edit Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

  <form  action="utilities/addNewInvoiceGeneralFunction.php" method="POST" enctype="multipart/form-data">
  
    <h1 class="username">Invoice : </h1>

    <div class="three-input-div dual-input-div">
    <p>Project</p>
      <select class="dual-input clean" placeholder="Project" id="product_name" name="product_name">
        <option value="">Please Select a Project</option>
        <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
        {
        ?>
        <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
        <?php echo $projectList[$cntPro]->getProjectName(); ?>
        </option>
        <?php
        }
        ?>
      </select>
    </div>

    <div class="three-input-div dual-input-div second-three-input">
      <p>To</p>
      <input required class="dual-input clean" type="text" placeholder="Auto Generated" readonly>
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->

    <div class="three-input-div dual-input-div">
      <p>Status (No. of Claims)</p>
      <select class="dual-input clean" name="status_of_claims" >
        <option value="">Please Select an Option</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>

    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" required>
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="three-input-div dual-input-div second-three-input">
      <p>Invoice</p>
      <select class="dual-input clean" name="invoice_name" >
        <option value="">Please Select an Option</option>
        <option value="Proforma">Proforma</option>
        <option value="Invoice">Invoice</option>
      </select>
    </div>
    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Invoice Type</p>
      <select class="dual-input clean" name="invoice_type" >
        <option value="">Please Select an Option</option>
        <option value="">none</option>
        <option value="Others">Others</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="three-input-div dual-input-div">
    <p>Item</p>
    <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item">
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Unit</p>
    <input class="dual-input clean" type="text" placeholder="Unit" id="unit" name="unit">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount">
  </div>
  <div class="tempo-two-input-clear"></div>
  <div class="three-input-div dual-input-div">
    <p>Item (Optional)</p>
    <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item2">
  </div>
  <div class="three-input-div dual-input-div second-three-input">
    <p>Unit</p>
    <input class="dual-input clean" type="text" placeholder="Unit" id="unit2" name="unit2">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount2">
  </div>
  <div class="tempo-two-input-clear"></div>
  <div class="three-input-div dual-input-div">
    <p>Item (Optional)</p>
    <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item3">
  </div>
  <div class="three-input-div dual-input-div second-three-input">
    <p>Unit</p>
    <input class="dual-input clean" type="text" placeholder="Unit" id="unit3" name="unit3">
  </div>  
  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount3">
  </div>

  <div class="tempo-two-input-clear"></div>
  <div class="three-input-div dual-input-div">
    <p>Item (Optional)</p>
    <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item4">
  </div>
  <div class="three-input-div dual-input-div second-three-input">
    <p>Unit</p>
    <input  class="dual-input clean" type="text" placeholder="Unit" id="unit4" name="unit4">
  </div>   
  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount4">
  </div>

  <div class="tempo-two-input-clear"></div>
  <div class="three-input-div dual-input-div">
    <p>Item (Optional)</p>
    <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item5">
  </div>
  <div class="three-input-div dual-input-div second-three-input">
    <p>Unit</p>
    <input class="dual-input clean" type="text" placeholder="Unit" id="unit5" name="unit5">
  </div>   
  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount5">
  </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>Include Service Tax (6%)</p>
      <input required class="" type="radio" value = "YES" name="charges" >Yes
      <input required class="" type="radio" value = "NO" name="charges" >No
    </div>

    <div class="tempo-two-input-clear"></div>

    <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

  </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>