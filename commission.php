<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$commissionDetails = getCommission($conn, "WHERE id =?",array("id"),array($_POST['id']), "s");
// $commissionDetails = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
$loanUid = $_POST['id'];
$projectName = $_POST['project_name'];
$status = 'COMPLETED';

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice | GIC" />
    <title>Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Commission</h1>
    <div class="short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
 		<div class="text-center">
            <img src="img/gic-logo.png" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
            <p class="invoice-address company-name"><b>GIC Consultancy Sdn. Bhd.</b></p>
            <p class="invoice-small-p">1189044-D (SST No : P11-1808-31038566)</p>
            <p class="invoice-address">1-2-42, Elit Avenue, Jalan Mayang Pasir 3, Bayan Lepas 11950 Penang.</p>
            <p class="invoice-address">Email: gicpenang@gmail.com  Tel:04-6374698</p>
        </div>
		<h1 class="invoice-title">Commission</h1>
        <div class="invoice-width50 top-invoice-w50">
        	<p class="invoice-p">
            	<b>Attn: <?php echo $commissionDetails[0]->getUpline(); ?></b>
            </p>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td><b>Commission No</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo date('Ymd', strtotime($commissionDetails[0]->getDateCreated())).$commissionDetails[0]->getID() ?></b></td>
                </tr>
                <tr>
                	<td>Project</td>
                    <td>:</td>
                    <td><?php echo $projectName  ?></td>
                </tr>
                <tr>
                	<td>Status</td>
                    <td>:</td>
                    <td>Forfeit</td>
                </tr>
                <tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo date('d/m/Y', strtotime($commissionDetails[0]->getDateCreated())) ?></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<th >No.</th>
                        <th >Items</th>
                        <th >Amount (RM)</th>
                    </tr>
            </thead>



                      <tr>
                      	<td class="td">1.</td>
                          <td class="td">Project Commission</td>
                        <td class="td"><?php echo $commissionDetails[0]->getCommission()  ?></td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                        <td class="td"></td>
                      <td class="td"></td>
                  </tr>
                  <tr>
                    <td class="td"></td>
                      <td class="td"></td>
                    <td class="td"></td>
                </tr>
                <tr>
                  <td class="td"></td>
                    <td class="td"></td>
                  <td class="td"></td>
              </tr>
              <tr>
                <td class="td"></td>
                  <td class="td"></td>
                <td class="td"></td>
            </tr>
        </table>
		<div class="clear"></div>
        <div class="invoice-width50 right-w50">
			<table class="invoice-bottom-small-table">
            	<tr>
                	<td>Sum Amount (excluding Service Tax)</td>
                    <td>:</td>
                    <td><?php echo $commissionDetails[0]->getCommission()  ?></td>
                </tr>
                <tr>
                	<td>Service Tax 6%</td>
                    <td>:</td>
                    <td>--</td>

                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td><b>Total Amount</b></td>
                    <td>:</td>
                    <td class="bottom-3rd-td border-td"><b><?php echo $commissionDetails[0]->getCommission()  ?></b></td>
                </tr>
            </table>

        </div>

        <div class="invoice-width50 left-w50">
			<table class="invoice-small-table">
            	<tr>
                	<td><b><u>Payee Details:</u></b></td>
                    <td><b>:</b></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Name</td>
                    <td><b>:</b></td>
                    <td><b>GIC HOLDING Sdn. Bhd.</b></td>
                </tr>
                <tr>
                	<td>Bank</td>
                    <td><b>:</b></td>
                    <td><b>Public Bank</b></td>
                </tr>
                <tr>
                	<td>Account No.</td>
                    <td><b>:</b></td>
                    <td><b>123456789</b></td>
                </tr>
            </table>
        </div>
        <div class="invoice-print-spacing"></div>
        <div class="signature-div">
        	<div class="signature-border"></div>
            <p class="invoice-p"><b>GIC Consultancy Sdn Bhd</b></p>
            <p class="invoice-p">Eddie Song</p>
        </div>
    </div>
	<div class="clear"></div>
    <div class="dual-button-div width100 same-padding">
    	<a href="#">
            <div class="left-button white-red-line-btn">
                Edit
            </div>
        </a>
    	<a href="#">
            <button class="right-button red-btn clean"  onclick="window.print()">
                Print
            </button>
        </a>
    </div>


</div>
<?php
if(isset($_POST['id']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($status)
    {
        array_push($tableName,"receive_status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }
  }
  array_push($tableValue,$loanUid);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"commission"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {}
 ?>



<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
