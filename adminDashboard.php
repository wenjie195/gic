<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);

$caseStatus = 'COMPLETED';
$loanDetails = getLoanStatus($conn, "WHERE case_status = ?",array("case_status"),array($caseStatus),"s");


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Dashboard</h1>
    <div class="short-red-border"></div>
	<div class="width100 overflow section-divider">
        <a href="admin2AddNewProject.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p g-first-3-p">NEW PROJECT</p>
            </div>
        </a>
        <a href="editInvoiceGeneral.php">
            <div class="five-red-btn-div left-mid-red">
                <p class="short-p five-red-p f-first-3-p">ISSUE INVOICE</p>
            </div>
        </a>
        <a href="#">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p e-first-3-p">ISSUE PAYROLL</p>
            </div> 
        </a>
 
        <!-- <a href="#">               
            <div class="five-red-btn-div right-mid-red">
                <p class="short-p issue-adv-p five-red-p">ISSUE ADVANCE</p>
            </div>
        </a>
        <a href="#">           
            <div class="five-red-btn-div">
                <p class="five-red-p">ISSUE COMMISSION</p>
            </div>  
        </a>   -->

    </div>    
    <div class="clear"></div>
	<div class="width100">
        <!-- <div class="red-dot"><p class="red-dot-p">15</p></div> -->
        <div class="red-dot"><p class="red-dot-p"><?php echo count($loanDetails);?></p></div>
    	<div class="big-rectangle" id="white-big-box">
        	<div class="left-side-title">
                <h3 class="rec-h3">
                    Advance</h3>
                <div class="short-red-border shorter"></div>
            </div>
            <div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a"  onclick="changeClass()">Show All</button>
            </div>
            <div class="clear"></div>
            <!-- repeat this div -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>
            <!-- end of repeat this div -->
            <!-- can delete -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>  
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Arte S Unit B17-0-12 SPA signed. Issue advance now.
                    </p>
                </div>
            </a>              
            <!-- end of can delete -->          
        </div>
    
    </div>
	<div class="width100">
    	<div class="red-dot"><p class="red-dot-p">25</p></div>
    	<div class="big-rectangle" id="white-big-boxC">
        	<div class="left-side-title">
                <h3 class="rec-h3">Commission</h3>
                <div class="short-red-border shorter"></div>
            </div>
            <div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a2"  onclick="changeClassC()">Show All</button>
            </div>
            <div class="clear"></div>
            <!-- repeat this div -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>
            <!-- end of repeat this div -->
            <!-- can delete -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>  
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Commission of Project Arte S shall be give out now.
                    </p>
                </div>
            </a>              
           <!-- end of can delete -->           
        </div>
    
    </div>
    
	<div class="width100">
    	<div class="red-dot"><p class="red-dot-p">35</p></div>
    	<div class="big-rectangle" id="white-big-boxI">
        	<div class="left-side-title">
                <h3 class="rec-h3">Invoice Follow-up</h3>
                <div class="short-red-border shorter"></div>
            </div>
            <div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a4"  onclick="changeClassI()">Show All</button>
            </div>
            <div class="clear"></div>
            <!-- repeat this div -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>
            <!-- end of repeat this div -->
            <!-- can delete -->
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>  
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>              
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>
            <a href="#">
                <div class="detailss-p red-color-hover">
                    <p class="small-date-p">1/1/2020 10:00 am</p>
                    <p class="contents-p">
                        Reminder: Follow-up Invoice No.19768
                    </p>
                </div>
            </a>              
           <!-- end of can delete -->           
        </div>
    
    </div>


</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
        
<script>
function changeClass() {
   var element = document.getElementById("white-big-box");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassC() {
   var element = document.getElementById("white-big-boxC");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a2").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassL() {
   var element = document.getElementById("white-big-boxL");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a3").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassI() {
   var element = document.getElementById("white-big-boxI");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a4").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
</body>
</html>
